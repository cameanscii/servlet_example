package com.javagda14.servlets.todos.service;

import com.javagda14.servlets.todos.model.AppUser;

import javax.enterprise.inject.Model;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Model
public class UserServiceImpl implements UserService{
    private List<AppUser> userList= new ArrayList<>();
    public void addUser(AppUser newUser){
        userList.add(newUser);
    }
    public List<AppUser> getUserList(){
        return userList;
    }

    @Override
    public void removeUserWithId(int id) {

        userList.removeIf(user -> user.getId() == id);

    }

    @Override
    public Optional<AppUser> getUserWithId(int userId) {
        return userList.stream().filter(user -> user.getId() == userId).findFirst();
    }
}
